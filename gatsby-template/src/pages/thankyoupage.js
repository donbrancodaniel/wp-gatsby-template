import React from "react"

import Layout from "../components/layout/layout"
import SEO from "../components/layout/seo"

const ThankYouPage = () => (
  <Layout>
    <SEO title="Thank you page" />
    <h1>Thank you for your order</h1>
    <p>You will be redirected to the home page in a x amount of time</p>
  </Layout>
)

export default ThankYouPage
