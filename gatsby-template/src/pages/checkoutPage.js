import React, { Component, useState, useEffect } from 'react'
import Helmet from "react-helmet"
import axios from 'axios'
import gql from 'graphql-tag'
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks'
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, FormText} from 'reactstrap'
import { StripeProvider, Elements } from 'react-stripe-elements'


// queries
import { GET_CART_ITEMS, GET_PRODUCT_IMAGE } from '../utils/queries'

// components
import Layout from '../components/layout/layout'
import StripePayment from '../components/stripe/StripePaymentForm'

// styles
import './common.css';


const defaultMargin = {
  margin: '50px auto 50px auto'
}

const centerStyle = {
  display: 'flex',
  flexDirection: 'column',
  // justifyContent: 'center',
  marginTop: '20px'
}

const marginTop100 = {
  marginTop: '100px'
}

const dividerStyle = {
  borderBottom: '1px solid #414141'
}

const listStyle = {
  display: 'flex',
  padding: '10px 0'
}


// ------------ create order overview list of items ------------ //

const CardList = ({ cardlist}) => {
  const list = cardlist.map((item, index) => {
    const { id, title, price, quantity } = item
    const { loading, error, data } = useQuery(GET_PRODUCT_IMAGE);
    // const image = graphql({query: GET_PRODUCT_IMAGE, variable:{id}})
    // console.log('is ther an image: ', useQuery(GET_PRODUCT_IMAGE))

    return (
      <Row key={index} style={listStyle} className="checkout__item">
        <Col xs={4}>{`${title}`}</Col>
        <Col xs={4}>{`${quantity}`}</Col>
        <Col xs={4}>{`${price}`}</Col>
      </Row>
    )
  })

  return list
}


// ------------ custom checkout form ------------ //

const CheckoutForm = ({ price }) => {

  const [ name, setName ] = useState()
  const [ email, setEmail ] = useState()
  const [ creditCard, setCreditcard ] = useState()
  const [ expirationDate, setexpirationDate ] = useState()
  const [ csv, setCSV ] = useState()


  // handle submission -- api request
  const submitCreditCardPayement = (e) => {
    e.preventDefault()
    const convertPrice = price * 100
    const send_price = parseInt(convertPrice.toString().replace('.', ''))
    const paymentDetails = {
      name: name,
      receipt_email: email,
      credit_card: creditCard,
      credit_card_expiration: expirationDate,
      csv: csv,
      price: send_price,
      currency: 'usd',
      payment_method: 'pm_card_visa',
      payment_method_types: ['card']
    }

    axios.post('http://wp.test/wp-json/api/payment/post/creditcard', {
      paymentDetails
    })
    .then(res => {
      const { status, action } = res.data

      if(status === 'succeeded') {
        // TODO: get language from apollo state
        const language = false
        const lang = language ? `${language}/` : ''
        window.location = `${window.location.origin}${lang}/thankyoupage`
      }

      if(status === 'requires_action') {
        // do something with action
        console.log('requires_action')
      }

      if(status === 'requires_payment_method') {
        console.log('failed')
      }
    })
    .catch(err => {
      console.error('error: ', err)
    })
  }

  return (
    <>
      <Form style={{margin: '30px auto'}} onSubmit={submitCreditCardPayement}>
        <FormGroup>
          <Label>Name</Label>
          <Input 
            type="text" 
            name="name" 
            id="checkout_name" 
            placeholder="John Doe" 
            onChange={e => setName(e.currentTarget.value)} 
          />
        </FormGroup>
        <FormGroup>
          <Label>Email</Label>
          <Input 
            type="email" 
            name="email" 
            id="checkout_email" 
            placeholder="johndoe@gmail.com" 
            onChange={e => setEmail(e.currentTarget.value)} 
          />
        </FormGroup>
        {/* Maybe payment method select option in the future */}
        {/* <FormGroup>
          <Label>Name</Label>
          <Input type="text" name="name" id="checkout_name" placeholder="John Doe">
        </FormGroup> */}
        <FormGroup>
          <Label>Credit card number</Label>
          <Input 
            type="number" 
            name="credit_card" 
            id="checkout_credit_card" 
            placeholder="4242 4242 4242" 
            onChange={e => setCreditcard(e.currentTarget.value)} 
          />
        </FormGroup>
        <FormGroup>
          <Label>Date</Label>
          <Input 
            type="date" 
            name="date" 
            id="checkout_date" 
            placeholder="01/21" 
            onChange={e => setexpirationDate(e.currentTarget.value)} 
          />
        </FormGroup>
        <FormGroup>
          <Label>CSV</Label>
          <Input 
            type="number" 
            name="csv" 
            id="checkout_csv" 
            placeholder="123" 
            onChange={e => setCSV(e.currentTarget.value)} 
          />
        </FormGroup>
        <FormGroup>
          <Button 
            type="submit"
            onSubmit={submitCreditCardPayement}
          >
            Pay €{price.replace('.', ',')}
          </Button>
        </FormGroup>
      </Form>
    </>
  )
}


// ------------ checkout template page ------------ //

const CheckoutPage = () => {
  const cart = useQuery(GET_CART_ITEMS)
  const { cartItems } = cart.data

  const [ formDetails, setFormSetedetails ] = useState({ name: null, email: null, address: null, card: null, })
  const [ totalPrice, setTotalPice ] = useState(false)
  const [ stripe, setStripe ] = useState(false)

  // componentDidMount
  // get stripe.js script / key from head
  useEffect(() => {
    if (window.Stripe) {
      setStripe(window.Stripe('pk_test_ixRrFL8zyf6cqUAtEAcXTjm300OUeR2LoB'))
    } else {
      document.querySelector('#stripe-js').addEventListener('load', () => {
        setStripe(window.Stripe('pk_test_ixRrFL8zyf6cqUAtEAcXTjm300OUeR2LoB'))
      });
    }
  }, [])

  // renders everytime cartItems is changed
  // optional in order to delete and recalculate the total amount
  useEffect(() => {
    const reducer = (accumulator, item) => {
      const cleanPrice  = item.price.replace('€', '')
      const total       = parseFloat(accumulator) + parseFloat(cleanPrice)
      return total
  }

    const calculatedTotalPrice = cartItems.reduce(reducer, 0)
    setTotalPice(calculatedTotalPrice.toFixed(2))
  },[cartItems])


  if(totalPrice && stripe) {
    return (
      <Layout>
        <Container>
          <Row>
            <Col xs={12} style={centerStyle, marginTop100}>
              <h2>Checkout page</h2>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={6} className="checkout__order-list" style={centerStyle}>
              <Row style={dividerStyle}>
                <Col xs={4}><div style={listStyle}><strong>Product</strong></div></Col>
                <Col xs={4}><div style={listStyle}><strong>Quantity</strong></div></Col>
                <Col xs={4}><div style={listStyle}><strong>Price</strong></div></Col>
              </Row>
              <CardList cardlist={cartItems} />
            </Col>
            <Col xs={12} md={6} className="checkout__order-form">
              <CheckoutForm price={totalPrice} order={cartItems} />
            </Col>
          </Row>
          <Row>
            <Col xs={12} >
              <StripeProvider stripe={stripe}>
                <Elements>
                  <StripePayment stripe={stripe} price={totalPrice}/>
                </Elements>
              </StripeProvider>
            </Col>
          </Row>
        </Container>
      </Layout>
    )
  } else {
    return <p>loading...</p>
  }
}

export default CheckoutPage