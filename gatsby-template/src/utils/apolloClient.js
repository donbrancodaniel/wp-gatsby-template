import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { CachePersistor } from 'apollo-cache-persist';
import fetch from 'isomorphic-fetch';

import config from './config';
import { resolvers, typeDefs } from './localState';

// Apollo Link is a simple yet powerful way to describe how you want to get the result of a GraphQL operation
const httpLink = createHttpLink({
  uri: config.debug ? config.graphQlUriDev : config.graphQlUri,
  fetch,
});

// cache
const cache = new InMemoryCache();

if (process.browser) {
  // save and restore your Apollo cache from persistent storage
  const persistor = new CachePersistor({
    cache,
    storage: window.localStorage,
    debug: config.debug,
  });
  persistor.restore();
}

const authLink = setContext(async (_, { headers }) => {
  const token = process.browser
    ? window.localStorage.getItem('token')
    : undefined;

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token || '',
    },
  };
});

// init client
const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
  typeDefs,
  resolvers,
});


cache.writeData({
  data: {
    isLoggedIn: !!localStorage.getItem('token'),
    cartItems: []
  },
});

export default client;
