//    Export all queries and mutation functions used
//    in order to keep the state of the application sync
//    and write cleaner less repatative code

import gql from 'graphql-tag'


// ---------------- get queries ----------------  //

// ----- user

export const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    isLoggedIn @client
  }
`;


// ----- product cart

export const GET_CART_ITEMS = gql`
  query GetCartItems {
    cartItems @client {
      id
      sku
      title
      price
      quantity 
    }
  }
`;

// export const GET_PRODUCT_IMAGE = gql`
//   query GetProductImageByID {
//     products {
//       nodes {
//         productId
//         image {
//           id
//           altText
//           uri
//         }
//         id
//       }
//     }
//   }
// `

export const GET_PRODUCT_IMAGE = gql`
  query GetProductImageByID($ID: String!) {
    wpgraphql {
      product(id: "cHJvZHVjdDoyOTU2") {
        image {
          id
          sourceUrl(size: LARGE)
          uri
          title
        }
      }
    }
  }
`




// ---------------- post mutations ---------------- //

export const ADD_CART_TO_ITEMS = gql`
  mutation GetCartItems {
    cartItems @client {
      id
      sku
      title
      price
      quantity 
    }
  }
`;
