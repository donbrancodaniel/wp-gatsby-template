module.exports = {
  debug: process.env.NODE_ENV === 'development',

  siteName: 'WP/GatsbyJs/Apollo Ecommerce',
  author: 'Daniel Koopmans',
  description:
    'A ecommerce system using WP, Gatsby, Apollo',
  siteUrl: 'http://localhost:8000/',
//   logo: '/images/logo-1024.png',
  graphQlUri: 'http://wp.test/graphql',
  graphQlUriDev: 'http://wp.test/graphql/',

//   homeBannerImage: '/images/home-bg-3.jpg',
  type: 'website',
  googleAnalytics: '',
//   backgroundColor: '#e0e0e0',
//   themeColor: '#c62828',

//   currency: '£',
  stripePublishableKey:
    process.env.NODE_ENV === 'development'
      ? 'pk_test_P0DEB2otulfya51U9lIkLXAn'
      : 'pk_live_eMN5tHGymDNn3DOZH8MX5ziD',
};
