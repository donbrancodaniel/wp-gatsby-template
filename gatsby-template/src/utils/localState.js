import { graphql } from "gatsby"

/**
 *
 * typeDefs structure local storage structure
 *
 **/

export const typeDefs = graphql`
  # get cart items with properties
  type getCartItems {
    products: [CartItem]
  }

  # add item to cart with properties
  type addItemToCart {
    success: Boolean!
    message: String
    products: [CartItem]
  }

  # remove item from cart
  type removeItemToCart {
    success: Boolean!
    message: String
    products: [CartItem]
  }

  # property of each product item
  type CartItem {
    id: ID!
    sku: String!
    title: String!
    price: Int!
    image: String
    quantity: Int!
    __typename: String!
  }

  type Query {
    getCartItems: getCartItems!
    isLoggedIn: Boolean!
    cartItems: [ID!]!
    # getCartItems(cartItem: String): Array
    # productCart(id: ID!): Launch --> returns a single product
  }

  extend type Query {
    isLoggedIn: Boolean!
  }

  type Mutation {
    addToCart: addItemToCart
    # addOrRemoveFromCart(id: ID!): [Launch]
    # removeFromCart: removeItemFromCart
  }

  # extend type Mutation {
  #   addOrRemoveFromCart(id: ID!): [CartItems]
  # }
`

/**
 *
 * Define resolvers in order to query the data from local storage
 *
 **/
export const resolvers = {
  // Query: {
  //   GetCartItems: () => {
  //     return cache.getCartItems()
  //   }
  //   // CartItems: (_, __, { cache }) =>
  //   //   cache.addToCart(),
  //   // CartItems: (cartItems) => {
  //   //   return cartItem
  //   // },
  // },
  // // missionPatch: (mission, { size } = { size: 'LARGE' }) => {
  // // return size === 'SMALL'
  // //   ? mission.missionPatchSmall
  // //   : mission.missionPatchLarge;
  // // }
}
