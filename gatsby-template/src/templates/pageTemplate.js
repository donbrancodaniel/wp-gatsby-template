import React from 'react'
import { Container } from 'reactstrap';

// import Helmet from 'react-helmet'

import Layout from '../components/layout/layout'
import FlexibleContent from './flexibleContent'

export default function ProductTemplate(page) {
  const { pageData, pageContent } = page.pageContext

  console.log('pageData: ', pageData)
  console.log('pageContent : ', pageContent)

  return (
    <Layout>
      <Container fluid={true}>
        <FlexibleContent flexibleContent={pageContent.acf.container} />
      </Container>
    </Layout>
  )
}
