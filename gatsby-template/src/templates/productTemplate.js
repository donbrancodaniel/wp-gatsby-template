import React, { useState } from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Container, Row, Col, TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
// import Helmet from 'react-helmet'

import Layout from '../components/layout/layout'
// import FlexibleContent from '../components/FlexibleContent'
import ExtraFlexibleContent from './extraFlexibleContent'

import Carousel from '../components/carousel'
import ProductDetailsCard from '../components/productDetailsCard'

export default function ProductTemplate(productData) {
  const { wcProductData, product } = productData.pageContext

  const details = product.acf.product_details_product[0]
  const extraContent = product.acf.extra_content

  // console.log('woocommerce: ', wcProductData)
  // console.log('acf : ', extraContent)

  const centerCardStyle = {
    display: 'flex',
    justifyContent: 'center',
    // marginTop: '-20vh',
    zIndex: '10',
  }

  return (
    <Layout>
      <Container fluid={true}>
        <Row>
          <Col xs="12" className="product__details">
            {/* Image gallary */}
            {/* <Carousel images={details.product_images}></Carousel> */}
          </Col>
          <Col xs="12" style={centerCardStyle} className="product_card_details">
            {/* Product details */}
            <ProductDetailsCard title={details.title} wcProductData={wcProductData} />
          </Col>
        </Row>
      </Container>

      {/* extra flexible product for each product} */}
      <ExtraFlexibleContent flexibleContent={extraContent} />

    </Layout>
  )
}


// export function GenerateTabs({ description, detailsList }) {
//   const [activeTab, setActiveTab] = useState('1');

//   const toggle = tab => {
//     if(activeTab !== tab) setActiveTab(tab);
//   }

//   function GenerateList() {
//     return detailsList.map(item => {
//       return (
//         <div>
//           <div>{ item.label }</div>
//           <div>{ item.value }</div>
//         </div>
//       )
//     })
//   }

//   return (
//     <Container>
//       <Row>
//         <Nav tabs>
//           <NavItem>
//             <NavLink
//               className={classnames({ active: activeTab === '1' })}
//               onClick={() => { toggle('1'); }}
//             >
//               Tab1
//             </NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink
//               className={classnames({ active: activeTab === '2' })}
//               onClick={() => { toggle('2'); }}
//             >
//               Moar Tabs
//             </NavLink>
//           </NavItem>
//         </Nav>
//         <TabContent activeTab={activeTab}>
//           <TabPane tabId="1">
//             <Row>
//               <Col sm="12">
//                 { ReactHtmlParser(description) }
//               </Col>
//             </Row>
//           </TabPane>
//           <TabPane tabId="2">
//             <Row>
//               <Col sm="12">
//                 {/* { GenerateList() } */}
//               </Col>
//             </Row>
//           </TabPane>
//         </TabContent>
//       </Row>
//     </Container>
//   )
// }

