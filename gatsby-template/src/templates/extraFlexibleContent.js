import React from 'react'

import ExtraDescription from '../components/acf-blocks/products/extraDescription'
import ExtraAdvancedList from '../components/acf-blocks/products/ExtraAdvancedList'

export default function extraFlexibleContent({ flexibleContent }) {

    return flexibleContent.map(block => {
        return block.extra_product_info.map((acf, index) => {

            switch (acf.acf_fc_layout) {
                case 'extra_description':
                    return <ExtraDescription key={index} content={acf}/>
            
                case 'extra_advanced_list':
                // ! NOTE SOMETHING COULD DIE IN BUILD IN HERE
                    console.log(acf)
                    return <ExtraAdvancedList key={index} content={acf}/>
            
                default:
                    console.log('default: ', acf)
                    return <h1>default</h1>
            }

        })
    });
}
