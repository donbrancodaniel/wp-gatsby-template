import React, { useState, useEffect } from 'react';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import gql from 'graphql-tag';

import { ADD_CART_TO_ITEMS, GET_CART_ITEMS, IS_LOGGED_IN } from '../utils/queries'


export default function ProductDetailsCard({ title, wcProductData }) {
  const { productId, sku, price, salePrice, stockQuantity, taxClass } = wcProductData
  const client = useApolloClient()

  const productCardStyle = {
    width: 'calc(100vw / 2)',
    maxWidth: '600px',
    padding: '20px',
    border: '1px solid black',
    borderRadius: '5px',
    backgroundColor: 'white',
  }
  const detailsStyle = {
    display: 'flex',
    marginBottom: '10px'
  } 
  const detailsLabelStyle = {
    width: '50%',
    padding: '5px 15px'
  } 
  const detailsValueStyle = {
    width: '50%',
    padding: '5px 15px'
  } 
  const detailsBuyStyle = {
    margin: 'auto',
    backgroundColor: 'blue',
    maxWidth: '250px',
    width: '100%',
    color: 'white',
    textAlign: 'center'
  }

  const addToCart = () => {  
    const data = client.readQuery({ query: GET_CART_ITEMS })

    // const { cartItems } = client.readQuery({ query: GET_CART_ITEMS })
    // console.log('check: ', cartItems)

    const newItem = {
      __typename: 'CartItem',
      id: productId,
      sku: sku,
      title: title,
      price: price,
      quantity: 1,
    
    };

    client.writeQuery({ 
      query: ADD_CART_TO_ITEMS, 
      data: { cartItems: [ ...data.cartItems, newItem ]}
    });
    // client.writeQuery({ query: { cartItems: [...cartItems, newItem] } })

  };
    
  return (
      <>
        <div style={productCardStyle}>
          <h1 className="product_title">{title}</h1>
          <div style={detailsStyle}>
            <div style={detailsLabelStyle} className="price">Price</div>
            <div style={detailsValueStyle}>{price}</div>
          </div>
          <div style={detailsStyle}>
            <div style={detailsLabelStyle} className="sale">sale</div>
            <div style={detailsValueStyle}>{salePrice}</div>
          </div>
          <div style={detailsStyle}>
            <div style={detailsLabelStyle} className="stock">stock</div>
            <div style={detailsValueStyle}>{stockQuantity}</div>
          </div>
          <div style={detailsStyle}>
            <div style={detailsLabelStyle} className="tax_class">Tax</div>
            <div style={detailsValueStyle} className="tax_class">{taxClass}</div>
          </div>
          <div style={detailsStyle}>
            <div 
              style={detailsBuyStyle} 
              className="product-purchase purchase-button"
              onClick={() => addToCart()}
            >
              Buy
            </div>
          </div> 
        </div>   
      </>
  )
}
