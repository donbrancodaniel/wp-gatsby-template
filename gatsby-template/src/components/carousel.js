import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import React from "react";
import Slider from "react-slick";
import Img from "gatsby-image"

const Carousel = (data) => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const carouselStyles = {
        height: 'calc(100vh - 50px)', 
        maxHeight: '100vh', 
        width: '100vw',
        maxWidth: '1400px',
        backgroundColor: 'red'
    }

    const sliderContent = data.images.map(image => {
        const img = image.localFile.childImageSharp.fluid.src
        
        return (
            <div className="carousel-outer">
                <div className="carousel-inner" style={{ backgroundImage: `url(${img})`}, carouselStyles } ></div>
                {/* <Img fluid={image.localFile.childImageSharp.fluid} /> */}
            </div>
        )
    })

    return (
        <Slider {...settings}>
            {sliderContent}
        </Slider>
    ); 
}

export default Carousel