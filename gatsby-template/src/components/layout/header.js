import React, { useState, useEffect } from "react"
import { useQuery, useApolloClient } from '@apollo/react-hooks';
// import gql from 'graphql-tag';
import { Link } from "gatsby"
import PropTypes from "prop-types"

import SearchBar from '../searchBar'

import  { GET_CART_ITEMS } from '../../utils/queries'

import "./hamburgers-master/dist/hamburgers.css"


// ------------------------------ styles -------------------------//
const h1Style = {
  marginLeft: `50px`,
  padding: `1.45rem 1.0875rem`,
}

const headerStyle = {
  background: `rebeccapurple`,
  marginBottom: `1.45rem`,
  display: `flex`,
  justifyContent: `space-between`,
}

const headerRight = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  alignSelf: 'center'
}

const cartStyle = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  alignSelf: 'center',
  height: '100%',
  margin: '0 40px',
  color: 'white',
}

const burgerPositionStyle = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  alignSelf: 'center',
  height: '100%',
  marginRight: '15px'
}

// ------------------------------ Hamburger -------------------------//
const HamburgerMenu = () => {
  const [collapse, setCollapse] = useState(false)
  let collapseMenu = collapse ? 'is-active' : ''

  return (
    <button 
      type="button" 
      style={burgerPositionStyle}
      className={`hamburger hamburger--collapse ${collapseMenu}`} 
      onClick={() => setCollapse(!collapse)}
    >
      <span className="hamburger-box">
        <span className="hamburger-inner"></span>
      </span>
    </button>
  )
}


// ------------------------------ Header -------------------------//

const Header = ({ siteTitle }) => {
  const cart = useQuery(GET_CART_ITEMS)
  const totalItems = cart.data.cartItems.length
  const checkoutPage = window.location.origin + '/checkoutpage'
  
  return (
    <header style={ headerStyle }>
      <div style={ h1Style }>
        <h1 style={{ margin: 0 }}>
          <Link
            to="/"
            style={{
              color: `white`,
              textDecoration: `none`,
            }}
          >
            {siteTitle}
          </Link>
        </h1>
      </div>
      <div style={headerRight}>
        <SearchBar />
        <div className="counter" style={cartStyle}><a href={checkoutPage}>{totalItems}</a></div>
        <HamburgerMenu/>
      </div>

      {/* <SearchBar /> */}
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
