import React, { Component, useState, useEffect } from 'react'
import axios from 'axios'
import { Button } from 'reactstrap'
import { 
  StripeProvider, 
  injectStripe, 
  useElements, 
  useStripe,
  Elements,
  CardElement, 
  CardCvcElement, 
  CardNumberElement,
  CardExpiryElement,
  PaymentRequestButtonElement,
  IbanElement,
  IdealBankElement,
  } from 'react-stripe-elements'


// * card styling
const stripeComponentStyle = {
  base: {
    fontSize: '16px',
    color: '#424770',
    '::placeholder': {
      color: '#aab7c4',
    }
  },
  invalid: {
    color: '#9e2146',
  }
}


// -------------- paymentform components / form ---------- //


// * Card Element: creditcard number, month && csv
const _CardElement = () => (
  <label>
    Card details
    <CardElement style={stripeComponentStyle} />
  </label>
)


// * Cardnumber Element
// @example: pass a prop function in order to update the input state
const _CardNumberElement = () => (
  <label>
    <CardNumberElement style={stripeComponentStyle} />
  </label>
)


// * CSV Element
// @example: pass a prop function in order to update the input state
const _CardCSV = () => (
  <label>
    <CardCvcElement style={stripeComponentStyle} />
  </label>
)


// * Expiry Element
// @example: pass a prop function in order to update the input state
const _CardExpiryElement = () => (
  <label>
    <CardExpiryElement style={stripeComponentStyle} />
  </label>
)


// * PaymentRequestButtonElement
// @example: pass a prop function in order to update the input state
const _PaymentRequestButtonElement = () => (
  <label>
    <PaymentRequestButtonElement style={stripeComponentStyle} />
  </label>
)


// * IbanElement
// @example: pass a prop function in order to update the input state
const _IbanElement = () => (
  <label>
    <IbanElement style={stripeComponentStyle} />
  </label>
)


// * IdealBankElement
// @example: pass a prop function in order to update the input state
const _IdealBankElement = () => (
  <label>
    <IdealBankElement style={stripeComponentStyle} />
  </label>
)


// -------------- stripe component ---------- //


// wrap component, because HOC wouldn't let you use props without it 
// --> CardElement, this.props.stripe
const CardForm          = injectStripe(_CardElement)
const CardNumber        = injectStripe(_CardNumberElement)
const CardCSV           = injectStripe(_CardCSV)
const CardExpiry        = injectStripe(_CardExpiryElement)
const Iban              = injectStripe(_IbanElement)
const IdealBank         = injectStripe(_IdealBankElement)
const PaymentRequestButton = injectStripe(_PaymentRequestButtonElement)


const StripePaymentForm = ({ stripe, price }) => {
  console.log('props: ', stripe)
  const [ success, setSuccess ] = useState(false)
  const [ error, setError ] = useState(false)
  const [ clientSecret, setClientSecret ] = useState(false)


  const submit = async (e) => {
    e.preventDefault()
    const convertPrice = price * 100
    const send_price = parseInt(convertPrice.toString().replace('.', ''))
    const paymentDetails = {
      name: "daniel",
      email: "daniel@gmail.com",
      credit_card: "creditCard",
      credit_card_expiration: "expirationDate",
      csv: "csv",
      price: 10000,
      currency: "usd",
      payment_method: "pm_card_visa",
      payment_method_types: ["card"]
    }

    axios.post('http://wp.test/wp-json/api/payment/post/payment_intent_card', {
        paymentDetails
      })
    .then(res => {
      if(res.status === 200) {
         console.log(res.data)
         window.location = window.location.origin + '/thankyoupage'
        //  setSuccess(true)
      }
    })
    .catch(err => {
      console.error(err)
      setError(true)
    })
  }

  // const confirmPayment = client_secret => {
  //   stripe.retrievePaymentIntent(client_secret).then(function(response) {
  //     if (response.error) {
  //       // Handle error here
  //     } else if (response.paymentIntent && response.paymentIntent.status === 'succeeded') {
  //       // Handle successful payment here
  //     }
  //   });
  //   // console.log('handleCardPayment : ', result)
  //   setSuccess(true)
  // }

  if (success) return <h1>Purchase Complete</h1>
  if (error) return <h1>shit went to hell</h1>

  if(!success) {
    return (
      <form onSubmit={submit}>
        <h2>Checkout details</h2>
          <CardForm style={stripeComponentStyle}/>     
        <Button 
          type="submit"
          onSubmit={submit}
        >
          Purchase
        </Button>
      </form>
    )
  }
}


export default StripePaymentForm







           {/* <Elements> 
              <CardNumber style={stripeComponentStyle}/> 
          </Elements>  */}
          {/*
          <Elements> 
              <CardExpiry style={stripeComponentStyle}/> 
          </Elements>  */}
          
          {/* <Elements>  */}
              {/* <CardCSV style={stripeComponentStyle}/> */}
          {/* </Elements>
          
          {/* @ iban is bitching about country  -- array returned is undefined */}
          {/* <Elements>
              <Iban style={stripeComponentStyle}/>
          </Elements> */}
  {/*         
          <Elements>
              <IdealBank style={stripeComponentStyle}/>
          </Elements> */}
          
          {/* <Elements> */}
              {/* <PaymentRequestButton 
          </Elements>
              paymentRequest={this.state.paymentRequest}
              style={stripeComponentStyle}/> */}