import React, { useState, useEffect, useRef } from "react"
import { StaticQuery } from "gatsby"
import { useQuery } from "@apollo/react-hooks"
import { Query } from "react-apollo"
import gql from "graphql-tag"
import Search from "gatsby-wordpress-search"

import { Input } from 'reactstrap'

const searchContainterStyle = {
  maxWidth: '400px',
  position: 'relative'
}

const hideStyle = {
  display: 'none',
  position: 'relative'
}

const displayStyle = {
  display: 'flex',
  flexDirection: 'column',
  position: 'absolute',
  width: '100%',
  marginTop: '-22px',
  padding: '15px',
  backgroundColor: '#74D5DD',
  zIndex: '1000',
}

const resultList = {
  padding: '5px 0'
}

const GET_SEARCH_RESULTS = graphql`
  query searchPages {
    allWordpressWpSearchResults {
      edges {
        node {
          id
          post_title
          searchData
          pathname
        }
      }
    }
  }
`

const SearchForMatches = (pages, input) => {
  const found = pages.map(( page, index ) => {
    const { post_title, searchData, pathname } = page.node
    const regex = /(<([^>]+)>)/ig

    const title        = post_title.toLowerCase()
    const searchString = searchData[0] ? searchData[0].toLowerCase().replace(regex, '') : ''

    if(title.includes(input) || searchString.includes(input)) {
      // ! The search is based on its origin of the gatsby server and not the WP server
      const link = pathname.replace('http://wp.test', window.location.origin)
      return <a href={link}><div key={index} style={resultList}>{ post_title }</div></a>
    }
  })

  return found
}


const SearchResultList = ({ result }) => {
  const [ allSearchResults, setSearchResults ] = useState(result)
  const [ foundPages, setFoundPages ] = useState(false)
  const [ searchInput, setSearchInput ] = useState('')

  useEffect(() => {
    if(searchInput !== '') {
      // generate list of results, based on search input
      const seachResults = SearchForMatches(allSearchResults, searchInput)
      if(seachResults) return setFoundPages(seachResults)
      if(!seachResults) return setFoundPages(<div>No Matches Found</div>)
    }

    // set to default if the input is empty
    if(searchInput === '' && foundPages) setFoundPages(false)
    
  }, [searchInput])

  return (
    <div style={searchContainterStyle}>
      <Input type="text" placeholder="Search for pages" value={searchInput} onChange={e => setSearchInput(e.currentTarget.value)} />
      <div style={ foundPages ?  displayStyle : hideStyle }>
        { foundPages ? foundPages : '' }      
      </div>
    </div>
  )
}


const SearchBar = () => (
  <StaticQuery
    query={GET_SEARCH_RESULTS}
    render={data => (
      <SearchResultList result={data.allWordpressWpSearchResults.edges} />
    )}
  />
)

export default SearchBar