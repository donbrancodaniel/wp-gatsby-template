import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import ReactHtmlParser from 'react-html-parser'
import Img from "gatsby-image"

export default function extraDescription({ content }) {
    const { details_list } = content
    const textStyle = {
        padding: '4px'
    }

    const detailsList = details_list.map((item, index) => {
        return (
            <Row key={index} style={textStyle}>
                <Col xs="4">{item.label}</Col>
                <Col xs="8">{item.value}</Col>
            </Row>
        )
    })

    return (
        <Container>
            <Row className="extra_description" >
                <Col xs="12">
                    { detailsList }
                </Col>
            </Row>   
        </Container>
    )  
}
