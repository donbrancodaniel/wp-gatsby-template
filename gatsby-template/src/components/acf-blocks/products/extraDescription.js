import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import ReactHtmlParser from 'react-html-parser'
import Img from "gatsby-image"

export default function extraDescription({ content }) {
    const { has_an_image, divider, wysiwyg, details_list, image, image_position } = content

    const textStyle = {
        padding: '50px'
    }

    if(has_an_image) {
        const rowDirection = !image_position ? 'row-reverse' : 'row'
        const direction = {
            display: 'flex',
            flexDirection: rowDirection,
            paddingTop: '100px',
            paddingBottom: '100px',
        }

        return (
            <Container>
                <Row style={direction} className="extra_description" >
                    <Col xs="12" md="4">
                        <img src={image.localFile.childImageSharp.fluid.src} />
                        {/* <Img fluid={image.localFile.childImageSharp.fluid.src} /> */}
                    </Col>
                    <Col xs="12" md="8" style={textStyle}>
                        { ReactHtmlParser(wysiwyg) }
                    </Col>
                </Row>   
            </Container>
        )
    } else {
        return (
            <Container>
                <Row>
                    <Col xs="12" md="8" style={textStyle}>
                        { ReactHtmlParser(wysiwyg) }
                    </Col>
                </Row>   
            </Container>
        )
    }
}
