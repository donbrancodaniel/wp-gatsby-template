module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `wp-gatsby template`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-lint-queries`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    // Setup WPGraphQL.com to be the source
    {
      resolve: `gatsby-source-graphql`,
      options: {
        // This type will contain remote schema Query type
        typeName: `WPGraphQL`,
        // This is field under which it's accessible
        fieldName: `wpgraphql`,
        // Url to query from
        url: `http://wp.test/graphql`,
      },
    },
    {
      resolve: 'gatsby-source-wordpress',
      options: {
        // includedRoutes: ["**/searchResults"],
        baseUrl: 'wp.test/',
        // WP.com sites set to true, WP.org set to false
        hostingWPCOM: false,
        protocol: 'http',
        // Use 'Advanced Custom Fields' Wordpress plugin
        useACF: true,
        auth: {},
        // Set to true to debug endpoints on 'gatsby build'
        verboseOutput: true,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-stripe`,
      options: {
        async: true,
      },
    },
    {
      resolve: `gatsby-plugin-stripe-checkout`,
      options: {
        async: true,
      },
    },
    {
    resolve: `@gatsby-contrib/gatsby-plugin-elasticlunr-search`,
      options: {
        // Fields to index
        fields: [`title`,],
        // How to resolve each field`s value for a supported node type
        resolvers: {
          // For any node of type MarkdownRemark, list how to resolve the fields` values
          wordpress__POSTS: {
            title: node => node.title,
            path: node => node.slug,
            tags: node => node.tags,
            keywords: node => node.acf.keywords,
          },
        },
      },
    },
  ],
}
