/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */


import wrapRoot from './src/utils/wrapRootElement'

export const wrapRootElement = wrapRoot;
