const createProducts = require('./gatsby/createProducts')
const createPages = require('./gatsby/createPages')

// create posts and pages
exports.createPages = async ({ actions, graphql }) => {
  await createProducts({ actions, graphql })
  // await createPages({ actions, graphql })
}
