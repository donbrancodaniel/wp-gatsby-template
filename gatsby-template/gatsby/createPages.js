const path = require(`path`)

module.exports = async ({ actions, graphql }) => {
    const GET_ALL_PAGES = `
        query GET_ALL_PAGES {
            allWordpressPage {
                edges {
                    node {
                        wordpress_id
                        slug
                        status
                        path
                        id
                    }
                }
            }
        }
    `

    const GET_PAGE_CONTENT = `
      query GET_PAGE_CONTENT($ID:String) {
        wordpressPage(id: {eq: $ID}) {
            title
            acf {
                container {
                    block {
                        acf_fc_layout
                        sub_title
                        title
                        image {
                            localFile {
                                childImageSharp {
                                    fluid {
                                    src
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
      }
    `

  const { createPage } = actions
  const postTemplate = path.resolve(`./src/templates/pageTemplate.js`)

  const fetchPages = async () =>
        await graphql(GET_ALL_PAGES).then(({ data }) => {
        return data.allWordpressPage.edges
    })

  const fetchPageACF = async (product_id) =>
        await graphql( GET_PAGE_CONTENT, {product_id} ).then(({ data }) => {
        return data.wordpressPage
    })

  const createPages = ({ pageData, pageContent }) => {
    createPage({
      path: `${pageData.path}`,
        component: postTemplate,
        context: { pageData, pageContent }
    })
  }

  await fetchPages().then(pages => {
    const env = process.env.NODE_ENV === 'production' ? 'production' : 'development'

    pages.map(async page => {
        const pageData = page.node
        const pageContent = await fetchPageACF(pageData.id)

        if(env === 'production' && pageData.status === 'publish') {
            createPages({ pageData, pageContent })
        } 
        
        if(env === 'development') {
            createPages({ pageData, pageContent })
        }
    })
  })
}
