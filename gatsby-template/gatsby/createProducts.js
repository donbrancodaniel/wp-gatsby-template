const path = require(`path`)

module.exports = async ({ actions, graphql }) => {
  const GET_WOOCOMMERCE_PRODUCTS = `
    query GET_ALL_PRODUCTS {
      wpgraphql {
        products {
          edges {
            node {
              id
              productId
              ... on WPGraphQL_SimpleProduct {
                id
                name
                backorders
                backordersAllowed
                catalogVisibility
                averageRating
                dateOnSaleFrom
                dateOnSaleTo
                description
                price
                onSale
                productId
                purchasable
                purchaseNote
                regularPrice
                salePrice
                shippingTaxable
                shortDescription
                sku
                slug
                status
                stockQuantity
                soldIndividually
                stockStatus
                taxClass
                taxStatus
                type
                totalSales
                weight
                width
                reviewsAllowed
                reviewCount
                manageStock
                menuOrder
                modified
                link
                length
                height
                featured
                date
              }
            }
          }
        }
      }
    }
  `

  const GET_ACF_FIELDS = `
    query GET_ALL_PRODUCTS($ID: Int) {
      allWordpressWpProduct(filter: {wordpress_id: {eq: $ID}}) {
        edges {
          node {
            id
            wordpress_id
            path
            status
            title
            acf {
              extra_content {
                extra_product_info {
                  acf_fc_layout
                  has_an_image
                  divider
                  image_position
                  wysiwyg
                  details_list {
                    label
                    value
                  }
                  image {
                    localFile {
                      childImageSharp {
                        fluid {
                          src
                        }
                      }
                    }
                  }
                }
              }
              product_details_product {
                ... on WordPressAcf_delivery_options {
                  id
                  delivery_text
                  delivery_regions {
                    base_price
                    region
                  }
                }
                ... on WordPressAcf_product_details {
                  id
                  title
                  short_description
                  main_product_image {
                    localFile {
                      childImageSharp {
                        fluid(maxWidth: 2560, quality: 100) {
                          src
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `

    // get images for gatsby-image library
    // ...GatsbySanityImageFluid

    //  product_images {
    //   localFile {
    //     childImageSharp {
    //       fluid(maxWidth: 2560, quality: 100) {
    //         src
    //       }
    //     }
    //   }
    // }
    
  const { createPage } = actions
  const postTemplate = path.resolve(`./src/templates/productTemplate.js`)

  const fetchWoocommerceProducts = async () =>
    await graphql( GET_WOOCOMMERCE_PRODUCTS ).then(({ data }) => {
      return data.wpgraphql.products.edges
    })

  const fetchProductACF = async (id) =>
    await graphql( GET_ACF_FIELDS, {id} ).then(({ data }) => {
      return data.allWordpressWpProduct.edges
    })

  const createPages = ({ wcProductData, product }) => {
    createPage({
      path: `${product.path}`,
        component: postTemplate,
        context: { wcProductData, product }
    })
  }

  await fetchWoocommerceProducts().then(woocommerceProducts => {
    const pages = process.env.NODE_ENV === 'production' ? 'production' : 'development'

    woocommerceProducts.map(async woocommerceProduct => {
      const wcProductData = woocommerceProduct.node
      const productContent = await fetchProductACF(wcProductData.productId)

      const status = productContent.map(content => {
        const product = content.node

        // ! Temporarily solution. Query is not accepting argument id
        if(product.wordpress_id === wcProductData.productId) {
  
          if(pages === 'production' && product.status === 'publish') {
            createPages({ wcProductData, product })
          }
          
          if(pages === 'development') {
            createPages({ wcProductData, product })
          }
        }

      })
    })
  })
}
